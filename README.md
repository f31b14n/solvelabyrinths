# SolveLabyrinths

> Un proyecto Python3

## Requerimientos
> python3<br>
> tkinter

``` bash
# Instalar tkinder linux
sudo apt-get install python3-tk
```

## Ejecución

``` bash
# Ejecutar
python3 run.py
```

## Mapas
> Los mapas del laberinto a solucionar se deben subir a maps<br>
> "#" para muros<br>
> "." para camino<br>
> "I" para el inicio<br>
> "S" para la salida<br>
> Todos los caracteres seprados por espacios

## Resultado
> Se pintan cuadros grises para los muros<br>
> Se pinta el inicio con un cuadro amarillo<br>
> Se pinta la salida con un cuadro verde<br>
> Los pasos dados se pintan en circulos naranjas