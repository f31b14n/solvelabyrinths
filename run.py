"""
@fileoverview   Buscar el camino para salir de un labereinto
@version        1.0

@author         1000721940 - Edwin Vasquez Acosta
                1014282882 - Nelson Zamudio Arias
                1030535093 - Anderson Mendez Barreto
                1022994885 - Yuly Alejandra Candela Heredia
                1026581356 - Karol Julieth Cárdenas Ceballos
                1033691656 - David Ernesto Castillo Sierra
                80798291   - Fabio Aldemar Ceballos Olmos
                1010222658 - Ximena Alexandra Cepeda Cepeda
                1010211302 - Cristian Esteban Cifuentes Gaitan
                1032471117 - DarvinsonEstewen Cortés García
                1022364011 - Cristian Camilo Cruz Urueña
                1013631136 - Michael AnyeloGomez Zapata
                1026559135 - Diego Alonso Guerrero Castro
                1019104107 - Luis Daniel Lemus Sanabria
                1022358620 - Cristian Daniel León Barón
                80738196   - Oscar Eduardo Olarte Velasco
                1015404703 - Diana Marcela Ospina Daza
                1012373495 - Aixa Yohana Padilla Mendoza
                1061741893 - Carlos Andrés Perafán Paz
                1016044917 - Elver Fabián Salinas Salinas
                80214724   - Julián Andrés Sarmiento Oyola
                1033759185 - Cristian David Walteros Ocampo
"""

import sys
import tkinter as tk


class Searched():
    """Direcciones donde se ha buscado"""
    t=False
    b=False
    l=False
    r=False

class Step():
    """Paso que se da, para realizar la busqueda"""
    before=None #Paso anterior
    def __init__(self, x, y):
        self.x=x
        self.y=y
        self.searched = Searched()

    def __repr__(self):
        """Representacion al hacerle print() a un objeto de esta clase"""
        return "Step({},{})".format(self.x, self.y)

class Labyrinth():
    start='I' #Caracter que representa el inicio
    exit='S' #Caracter que representa la salida
    wall='#' #Caracter que representa un muro
    pixel=10 #Tamaño de los cuadros
    cv=None #Variable que almancena el canvas

    def __init__(self, file):
        """Funcion constructora

        @param {String} file  => ruta del archivo con el mapa del laberinto
        """
        self.middle = self.pixel/2
        self.readFile(file)
        self.window = tk.Tk() #Crea la ventana

    def readFile(self, file):
        """Lee el archivo mapa
        Y busca el punto de inicio

        @param {String} file  => ruta del archivo con el mapa del laberinto
        """
        self.lab = open(file, "r") #Abre el archivo
        self.lab = self.lab.readlines() #Lee las lineas (filas) del archivo
        self.height = len(self.lab) #Cantidad de filas
        for i in range(self.height): #Recorre las filas
            self.lab[i] = self.lab[i].replace('\n', '').split(' ') #Lee las columnas

            #Busca el cuadro de inicio
            try:
                start_in = self.lab[i].index(self.start)
            except Exception as e:
                pass
            else:
                self.start_in = Step(start_in,i)
        self.width = len(self.lab[0]) #Cantidad de columnas

    def drawCube(self, x, y, color="grey"):
        """Pinta un muro en el canvas
        
        @param {Integer} x
        @param {Integer} y
        @param {String} color  => color del background
        """
        coor = (x, y, x+self.pixel, y+self.pixel)
        self.cv.create_rectangle(coor, fill=color)

    def drawText(self, x, y, text):
        """Pinta un texto en el canvas
        
        @param {Integer} x
        @param {Integer} y
        @param {String} text
        """
        self.cv.create_text(x+self.middle,y+self.middle, text=text)

    def drawStep(self, x, y, color="orange", border="black"):
        """Pinta un paso en el canvas

        @param {Integer} x
        @param {Integer} y
        @param {String} color  => color del background
        @param {String} border => color del borde
        """
        r=3
        cx = x*self.pixel+self.middle
        cy = y*self.pixel+self.middle
        self.cv.create_oval(cx-r, cy-r, cx+r, cy+r, fill=color, outline=border)

    def deleteStep(self, step):
        """Borra un objeto Step
        Borra el dibujo del paso en el canvas

        @param {Object} step  => Step
        """
        self.drawStep(step.x, step.y, color="grey", border="white")
        del step

    def startDraw(self):
        """Inicia el dibujo del laberinto, en canvas
        """
        width = self.width*self.pixel #Ancho del canvas
        height = self.height*self.pixel #Altura del canvas
        self.window.geometry("{}x{}".format(width+10,height+10)) #Tamaño de la ventana
        self.window.title("Laberinto")

        self.cv = tk.Canvas(self.window, bg="white", height=height, width=width) #Crea el canvas
        self.cv.pack() #Empaqueta el cambas

        # Pintar mapa
        for y in range(self.height): # Recorrer filas
            for x in range(self.width): #Recorrer columnas
                cx = x*self.pixel
                cy = y*self.pixel
                if self.lab[y][x] == self.wall:
                    self.drawCube(cx,cy) #Pinta Muro
                elif self.lab[y][x] == self.start:
                    self.drawCube(cx,cy, color="yellow") #Pinta el punto de inicio
                elif self.lab[y][x] == self.exit:
                    self.drawCube(cx,cy, color="green") #Pinta el punto de salida

    def endDraw(self):
        """Lanza la venta"""
        self.window.mainloop()

    def solveByDepth(self, step):
        """Busca la solucion del laberinto en el paso actual,
        De no encontrar la solucion en el paso actual,
        Se llama a si misma para buscar la solucion en el siguiente paso

        @param {Object} step => Step
        @return {Boolean}
        """
        def getStep(x,y):
            """Verifica que el siguiente paso sea valido
            Si es valido, llama la funcion solveByDepth
            con el nuevo paso, para continuar con la busqueda

            @param {Integer} x
            @param {Integer} y
            @return {Boolean}
            """
            if x < 0 or x >= self.width or y < 0 or y >= self.height:
                return False #la siguiente posicion no esta dentro de la grafica
            if step.before != None and step.before.x == x and step.before.y == y:
                return False #la siguiente posicion es la posicion del paso anterior
            if self.lab[y][x] == self.wall:
                return False
            next_step = Step(x,y) # Crea la instancia de la clase Step, crea el nuevo paso a dar
            next_step.before = step # Setea al nuevo paso, su paso anterior (El actual)
            return self.solveByDepth(next_step) # Buscar solucion en nuevo paso

        def getCoor(find_in):
            """Retorna las coordenadas del siguiente paso
            Segun la direccion que se esta evaluando

            @param {String} find_in  => Direccion hacia donde see va a buscar
            @return {dict}
            """
            if find_in == 'r':
                return dict(x=step.x+1, y=step.y)
            if find_in == 't':
                return dict(x=step.x, y=step.y-1)
            if find_in == 'l':
                return dict(x=step.x-1, y=step.y)
            if find_in == 'b':
                return dict(x=step.x, y=step.y+1)

        def find(find_in):
            """Busca la solucion en la direccion que se esta evaluando

            @param {String} find_in  => Direccion hacia donde see va a buscar
            @return {Boolean|None}
            """
            if getattr(step.searched, find_in) == False:
                setattr(step.searched, find_in, True)
                coor = getCoor(find_in)
                if getStep(coor['x'], coor['y']):
                    return True

        print(step) # Imprime en consola el paso que se esta evaluando actualmente
        self.drawStep(step.x, step.y) # Pinta el paso que see esta evaluando actualmente

        val = self.lab[step.y][step.x] #Caracter del mapa, en las coordenadas actuales
        if val == self.exit:
            print('OK salida')
            return True #El caracter es el caracter de salida

        if find('r'): #Find Right
            return True
        if find('b'): #Find Bottom
            return True
        if find('t'): #Find Top
            return True
        if find('l'): #Find Left
            return True

        before = step.before
        if before == None:
            print('No hay solucion')
            return True #Se devolvio al paso inicial y no encontro solucion

        self.deleteStep(step) #Borra el paso actual, porque no tiene solucion
        return self.solveByDepth(before) #Se devuelve un paso, porque no tiene solucion

def main():
    """Inicializa la la aplicacion"""
    lab = Labyrinth('maps/5.txt')
    lab.startDraw()
    lab.solveByDepth(lab.start_in)
    lab.endDraw()

if __name__ == '__main__': #Punto de inicio de la ejecucion
    sys.setrecursionlimit(20000) #Aumenta el limite de recursividad
    main()